import { Component } from '@angular/core';
import { UserService } from './service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Practice1';
  user: any;
  constructor(SE: UserService) {
    this.user = SE.getUser();
  }
  currency = 10000;
  name = 'hello';
  name2 = 'GANGSTER';
  decimalData = 1.234567;
  jsonData = { name: 'sidharth' };
  dateValue = Date();
  disabled: boolean = false;
  data = [
    {
      name1: 'Kaarthi',
      age1: 23,
    },
    {
      name2: 'aravinth',
      age2: 24,
    },
  ];
}
