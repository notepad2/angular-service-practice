import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor() {}
  Counter: number = 100;
  addone() {
    this.Counter++;
  }

  getUser() {
    return [
      {
        name: 'kaarthi',
        age: 23,
      },
      {
        name: 'yoges',
        age: 24,
      },
      {
        name: 'siddharth',
        age: 25,
      },
      {
        name: 'suriya',
        age: 48,
      },
      {
        name: 'thala',
        age: 50,
      },
    ];
  }
}
