import { Component } from '@angular/core';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-customer2',
  templateUrl: './customer2.component.html',
  styleUrls: ['./customer2.component.css'],
})
export class Customer2Component {
  constructor(public _maths: UserService) {}
  Increaseone() {
    this._maths.addone();
  }
}
