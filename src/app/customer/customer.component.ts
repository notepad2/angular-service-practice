import { Component } from '@angular/core';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
})
export class CustomerComponent {
  data: any;
  constructor(SET: UserService) {
    this.data = SET.getUser();
  }
}
