import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerComponent } from './customer/customer.component';
import { TwoWayComponent } from './two-way/two-way.component';
import { FormsModule } from '@angular/forms';
import { CheckngifComponent } from './checkngif/checkngif.component';
import { UserService } from './service/user.service';
import { Customer2Component } from './customer2/customer2.component';
import { SqrtPipe } from './sqrt.pipe';
import { ChildComponent } from './child/child.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent,
    TwoWayComponent,
    CheckngifComponent,
    Customer2Component,
    SqrtPipe,
    ChildComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [UserService],
  bootstrap: [AppComponent],
})
export class AppModule {}
