import { Component } from '@angular/core';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-checkngif',
  templateUrl: './checkngif.component.html',
  styleUrls: ['./checkngif.component.css'],
})
export class CheckngifComponent {
  check: boolean = false;
  data2 = '';

  numarray: number[] = [1, 2, 3, 4, 66, 88];

  colorSelected: string = 'yellow';

  constructor(public _maths: UserService) {}
}
