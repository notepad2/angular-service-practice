import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckngifComponent } from './checkngif.component';

describe('CheckngifComponent', () => {
  let component: CheckngifComponent;
  let fixture: ComponentFixture<CheckngifComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CheckngifComponent]
    });
    fixture = TestBed.createComponent(CheckngifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
